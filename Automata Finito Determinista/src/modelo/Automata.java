
package modelo;

/**
 *
 * @author ANGIE | anderssonccg
 */
public class Automata {
    private String cadena;

    public Automata() {
        
    }
    
    public Automata(String cadena) {
        this.cadena = cadena;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }
    
    public boolean verificar(){
        if(this.cadena == null || this.cadena.equals("")) throw new RuntimeException("Debe digitar la cadena");
        return (this.cadena.length()%2 == 0 && this.verificarCaracteres()) || this.cadena.equals(" ");
    }
    
    private boolean verificarCaracteres(){
        for(int i = 0; i < this.cadena.length(); i++){
           if(this.cadena.charAt(i) != 'a') return false;
        }
        return true;
    }
}
